﻿// TripleXGame.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <ctime>

using namespace std;

void PrintIntroduction(int LevelDifficulty)
{
    cout << "You are a secret agent breaking into a " << LevelDifficulty << " secure server room...";
    cout << endl;
    cout << "Enter the correct code to continue..." << endl;
}

bool PlayGame(int LevelDifficulty)
{
    PrintIntroduction(LevelDifficulty);

    const int CodeA = rand() % LevelDifficulty + LevelDifficulty;
    const int CodeB = rand() % LevelDifficulty + LevelDifficulty;
    const int CodeC = rand() % LevelDifficulty + LevelDifficulty;

    const int CodeSum = CodeA + CodeB + CodeC;
    const int CodeProduct = CodeA * CodeB * CodeC;

    //Print sum and product to the terminal
    cout << endl;
    cout << "There are 3 numbers in the code" << endl;
    cout << "The codes add-up to: " << CodeSum << endl;
    cout << "The codes multiply to give: " << CodeProduct << endl;

    int GuessA, GuessB, GuessC;
    cin >> GuessA >> GuessB >> GuessC;

    int GuessSum = GuessA + GuessB + GuessC;
    int GuessProduct = GuessA * GuessB * GuessC;

    if (GuessSum == CodeSum && GuessProduct == CodeProduct)
    {
        cout << "\n*** Well done agent! You have extracted the file! Keep going! ***\n" << endl;
        return true;
    }
    else
    {
        cout << "\n*** You entered the wrong file! Careful agent! Try again ***\n" << endl;
        return false;
    }
}

int main()
{

    int LevelDifficulty = 1;
    const int MaxLevel = 10;

    //Spawn random intergers sequence
    srand(time(NULL));

    while (LevelDifficulty <= MaxLevel)
    {
        bool bLevelCompleted = PlayGame(LevelDifficulty);
        cin.clear();    //Fix flow errors
        cin.ignore();   //Discards the buffer 

        if (bLevelCompleted)
        {
            ++LevelDifficulty;
        }
    }

    cout << "Congratulations! You completed the game!" << endl;
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
